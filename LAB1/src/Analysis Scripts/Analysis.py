# -*- coding: utf-8 -*-
"""
Created on Tue Feb  8 22:10:59 2022

@author: ZMY
"""

import pandas as pd
import matplotlib.pyplot as plt

Stationary_data = pd.read_csv('C:/Users/89327/Desktop/Stationary_data.csv')
Moving_data = pd.read_csv('C:/Users/89327/Desktop/Moving_data.csv')
Driving_data = pd.read_csv('C:/Users/89327/Desktop/Driving_data.csv')

xS = Stationary_data['field.utm_northing']
yS = Stationary_data['field.utm_easting']

plt.plot(xS,yS,marker='.')
plt.show()

xM = Moving_data['field.utm_northing']
yM = Moving_data['field.utm_easting']

plt.plot(xM,yM,marker='.')
plt.show()

xD = Driving_data['field.utm_northing']
yD = Driving_data['field.utm_easting']

plt.plot(xD,yD,marker='.')
plt.show()