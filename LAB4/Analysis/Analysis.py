# -*- coding: utf-8 -*-
"""
Spyder Editor

This is a temporary script file.
"""

import os
import yaml
import matplotlib.pyplot as plt
from math import atan2, sqrt, cos, sin, asin, radians
import numpy as np
from scipy import signal, integrate
#%% GPS LOOP
def haversine(lat1, lon1, lat2, lon2):
    R = 6371  # earth radius

    dLat = radians(lat2 - lat1)
    dLon = radians(lon2 - lon1)
    lat1 = radians(lat1)
    lat2 = radians(lat2)

    a = sin(dLat / 2) ** 2 + cos(lat1) * cos(lat2) * sin(dLon / 2) ** 2
    c = 2 * asin(sqrt(a))

    return R * c

with open(os.path.join('/Users/89327/Desktop/New folder/Imu_Loop.yaml')) as f:
    data = yaml.safe_load_all(f)
    ls = list(data)
    x_accel = []
    y_accel = []
    z_accel = []
    imu_time = []
    itime = []

    for i in range(2000, 18280):
        x_accel.append(ls[i]['linear_acceleration']['x'])
        y_accel.append(ls[i]['linear_acceleration']['y'])
        z_accel.append(ls[i]['linear_acceleration']['z'])
        imu_time.append(i/40-50)
    for i in range(0, len(ls)-1):
        itime.append(i/40)

x_off = np.mean(x_accel)
y_off = np.mean(y_accel)
z_off = np.mean(z_accel)

x_cor = []
y_cor = []
z_cor = []
for i in range(0, len(x_accel)):
    x_cor.append(x_accel[i] - x_off)
    y_cor.append(y_accel[i] - y_off)
    z_cor.append(z_accel[i] - z_off)

a2 = []
for i in range(0, len(x_accel)):
    a2.append(sqrt((x_accel[i] - x_off) ** 2 + (y_accel[i] - y_off) ** 2) + (z_accel[i] - z_off) ** 2)

time = np.linspace(0, imu_time[-1], 16279)

v2 = integrate.cumtrapz(a2, imu_time)

s = integrate.cumtrapz(v2, time)


vs = []
for i in range(1, len(s)):
    vs.append((s[i] - s[i - 1]) / 0.025)

vs = signal.detrend(vs)
zero = [0]*2001
vss = []
vss.extend(zero)
vss.extend(vs)
zero = [0]*1246
vss.extend(zero)

for i in range(0, len(vss)):
    if vss[i] < 0:
        vss[i] = -vss[i]

# plt.plot(imu_time, x_cor, label='x acceleration')
# plt.plot(imu_time, y_cor, label='y acceleration')
# plt.plot(imu_time, z_cor, label='z acceleration')
# plt.xlabel('Time', fontsize=16)
# plt.ylabel('Acceleration', fontsize=16)
# plt.title('Acceleration w/o bias', fontsize=20)
# plt.legend()
# plt.show()

# Calculate an estimate of the velocity from GPS measurements.
with open(os.path.join('/Users/89327/Desktop/New folder/Gps_Loop.yaml')) as f:
    data = yaml.safe_load_all(f)
    ls = list(data)
    lat = []
    lon = []
    gps_time = []

    for i in range(0, len(ls)-1):
        lat.append(ls[i]['latitude'])
        lon.append(ls[i]['longitude'])
        gps_time.append(i)

distance = []
for i in range(1, len(ls)-1):
    distance.append(haversine(lat[i], lon[i], lat[i-1], lon[i-1]) * 1000)  # default unit is in km


v3 = []
for i in range(0, len(distance)):
    v3.append(distance[i]/1)

gtime = np.linspace(0,503,503)

# plt.plot(time, v2, label='imu_before')
# plt.plot(gtime, v3, label='gps')
# plt.xlabel('Time (s)', fontsize=16)
# plt.ylabel('Velocity (m/s)', fontsize=16)
# plt.title('Velocity based on acceleration w/o bias', fontsize=20)
# plt.show()


# plt.plot(itime, vss, label='imu_after')
# plt.plot(gtime, v3, label='gps')
# plt.xlabel('Time (s)', fontsize=16)
# plt.ylabel('Velocity (m/s)', fontsize=16)
# plt.title('Velocity based on gps', fontsize=20)
# plt.legend()
# plt.show()
#%% ORIGINAL IMU CIRCLE
filenameIC = os.path.join(os.path.dirname(__file__),'Imu_Circle.yaml').replace("\\","/")
IC = list(yaml.safe_load_all(open(filenameIC)))
filenameMC = os.path.join(os.path.dirname(__file__),'Mag_Circle.yaml').replace("\\","/")
MC = list(yaml.safe_load_all(open(filenameMC)))
accx = []
accy = []
accz = []
angx = []
angy = []
angz = []
magx = []
magy = []
magz = []
yawc = []
timeic = []
timemc = []
for i in range(0, len(IC)-1):
    accx.append(float(IC[i]['linear_acceleration']['x']))
    accy.append(float(IC[i]['linear_acceleration']['y']))
    accz.append(float(IC[i]['linear_acceleration']['z']))
    angx.append(float(IC[i]['angular_velocity']['x']))
    angy.append(float(IC[i]['angular_velocity']['y']))
    angz.append(float(IC[i]['angular_velocity']['z']))
    yawc.append(float(IC[i]['yaw']))
    timeic.append(i/40)
for i in range(0, len(MC)-1):
    magx.append(float(MC[i]['magnetic_field']['x']))
    magy.append(float(MC[i]['magnetic_field']['y']))
    magz.append(float(MC[i]['magnetic_field']['z']))
    timemc.append(i/40)

# plt.figure(figsize=(15,10))
# plt.scatter(magx, magy, marker='+')
# plt.xlabel('Magnetic_Field_x', fontsize=15)
# plt.ylabel('Magnetic_Field_y', fontsize=15)
# plt.suptitle('Original Magnetic_Field Values', fontsize=20)
# plt.savefig('Original Magnetic_Field Values.png')
# plt.show()

# acc_mx = np.mean(accx)
# acc_my = np.mean(accy)
# acc_mz = np.mean(accz)
# ang_mx = np.mean(angx)
# ang_my = np.mean(angy)
# ang_mz = np.mean(angz)

# cal_accx = []
# cal_accy = []
# cal_accz = []
# cal_angx = []
# cal_angy = []
# cal_angz = []

# for i in range(0,len(timeic)):
#     cal_accx.append(accx[i] - acc_mx)
#     cal_accy.append(accy[i] - acc_my)
#     cal_accz.append(accz[i] - acc_mz)
#     cal_angx.append(angx[i] - ang_mx)
#     cal_angy.append(angy[i] - ang_my)
#     cal_angz.append(angz[i] - ang_mz)

calibration = [(max(accx)+min(accx))/2, (max(accy)+min(accy))/2, (max(accz)+min(accz))/2, (max(angx)+min(angx))/2, (max(angy)+min(angy))/2, (max(angz)+min(angz))/2, (max(magx)+min(magx))/2, (max(magy)+min(magy))/2, (max(magz)+min(magz))/2]

cal_accx = [x - calibration[0] for x in accx]
cal_accy = [y - calibration[1] for y in accy]
cal_accz = [z - calibration[2] for z in accz]
cal_angx = [x - calibration[3] for x in angx]
cal_angy = [y - calibration[4] for y in angy]
cal_angz = [z - calibration[5] for z in angz]
cal_magx = [x - calibration[6] for x in magx]
cal_magy = [y - calibration[7] for y in magy]
cal_magz = [z - calibration[8] for z in magz]

# plt.figure(figsize=(15,10))
# plt.axes(aspect='equal')
# plt.scatter(cal_magx, cal_magy, marker='+')
# plt.xlabel('Magnetic_Field_x', fontsize=15)
# plt.ylabel('Magnetic_Field_y', fontsize=15)
# plt.suptitle('Magnetic_Field Values After Hard-Iron Calibrated', fontsize=20)
# plt.savefig('Magnetic_Field Values After Hard-Iron Calibrated.png')
# plt.show()

radius = []
semi_major = []
semi_minor = []
r = []
for i in range(0,len(cal_magx)):
    radius.append(sqrt(cal_magx[i]**2 + cal_magy[i]**2))
    r.append(sqrt(cal_magx[i]**2 + cal_magy[i]**2))

for i in range(0,4567):
    semi_major.append(max(r))
    semi_minor.append(min(r))
    r.remove(semi_major[i])
    r.remove(semi_minor[i])

index_major = radius.index(semi_major[30])
index_minor = radius.index(semi_minor[30])

theta = atan2(cal_magy[index_major],cal_magx[index_major])
R = [[cos(theta), sin(theta)], [-sin(theta), cos(theta)]]
cal_magx, cal_magy = np.matmul(R, [cal_magx, cal_magy])

# plt.figure(figsize=(15,10))
# plt.axes(aspect='equal')
# plt.scatter(cal_magx, cal_magy, marker='+')
# plt.xlabel('Magnetic_Field_x', fontsize=15)
# plt.ylabel('Magnetic_Field_y', fontsize=15)
# plt.suptitle('Magnetic_Field Values After Soft-Iron Calibrated(Rotation)', fontsize=20)
# plt.savefig('Magnetic_Field Values After Soft-Iron Calibrated(Rotation).png')
# plt.show()

scaler = semi_minor[2283]/semi_major[2284]
for i in range(0,len(cal_magx)):
    cal_magx[i] = cal_magx[i]*scaler

# plt.figure(figsize=(15,10))
# plt.axes(aspect='equal')
# plt.scatter(cal_magx, cal_magy, marker='+')
# plt.xlabel('Magnetic_Field_x', fontsize=15)
# plt.ylabel('Magnetic_Field_y', fontsize=15)
# plt.suptitle('Magnetic_Field Values After Soft-Iron Calibrated(Rescaling)', fontsize=20)
# plt.savefig('Magnetic_Field Values After Soft-Iron Calibrated(Rescaling).png')
# plt.show()

R = [[cos(theta), -sin(theta)], [sin(theta), cos(theta)]]
cal_magx, cal_magy = np.matmul(R, [cal_magx, cal_magy])

t = np.linspace(0, 360, 360)
x = 0.16 * np.cos(np.radians(t))
y = 0.16 * np.sin(np.radians(t))

# plt.figure(figsize=(15,10))
# plt.axes(aspect='equal')
# plt.plot(x, y, color='r')
# plt.scatter(cal_magx, cal_magy, marker='+')
# plt.xlabel('Magnetic_Field_x', fontsize=15)
# plt.ylabel('Magnetic_Field_y', fontsize=15)
# plt.suptitle('Magnetic_Field Values After Soft-Iron Calibrated(Final)', fontsize=20)
# plt.savefig('Magnetic_Field Values After Soft-Iron Calibrated(Final).png')
# plt.show()

# yawnc = []
# pitchnc = []
# rollnc = []
# for i in range(0,len(timeic)):
#     pitchnc.append(atan2(cal_accx[i],sqrt(cal_accy[i]**2+cal_accz[i]**2)))
#     rollnc.append(atan2(cal_accy[i],sqrt(cal_accx[i]**2+cal_accz[i]**2)))
#     yawnc.append(atan2(-cal_magy[i]*cos(rollnc[i])+cal_magz[i]*sin(rollnc[i]),(cal_magx[i]*cos(pitchnc[i])+cal_magy[i]*sin(pitchnc[i])*sin(rollnc[i])+cal_magz[i]*sin(pitchnc[i])*cos(rollnc[i]))))

# yawnc.pop()
# yaw_from_mag = np.unwrap(yawnc)

yaw_from_mag = []
for i in range(0,len(timeic)-1):
    yaw_from_mag.append(atan2(-cal_magy[i],cal_magx[i]))

yaw_from_mag = np.unwrap(yaw_from_mag)

yaw_from_gyro = integrate.cumtrapz(angz, timeic)
yaw_from_gyro = np.unwrap(yaw_from_gyro)

time = []
for i in range(0,len(timeic)-1):
    time.append(timeic[i])

# plt.figure(figsize=(15,10))
# plt.plot(time, yaw_from_mag, label='Magnetic_Field')
# plt.plot(time, yaw_from_gyro, label='GYRO')
# plt.xlabel('Time(s)', fontsize=15)
# plt.ylabel('YAW Angle(rad)')
# plt.legend()
# plt.suptitle('YAW Angle From Magnetic_Field And GYRO Values', fontsize=20)
# plt.savefig('YAW Angle From Magnetic_Field And GYRO Values.png')
# plt.show()

hb, ha = signal.butter(1, 0.000005, 'highpass', fs=40)
lb, la = signal.butter(1, 0.5, 'lowpass', fs=40)

yaw_from_mag_fil = signal.filtfilt(lb,la,yaw_from_mag)
yaw_from_gyro_fil = signal.filtfilt(hb,ha,yaw_from_gyro)

# plt.figure(figsize=(15,10))
# plt.plot(time, yaw_from_mag_fil, label='Magnetic_Field')
# plt.plot(time, yaw_from_gyro_fil, label='GYRO')
# plt.xlabel('Time(s)', fontsize=15)
# plt.ylabel('YAW Angle(rad)')
# plt.legend()
# plt.suptitle('YAW Angle After Being Filtered', fontsize=20)
# plt.savefig('YAW Angle After Being Filtered.png')
# plt.show()

yaw_com = (0.2*yaw_from_gyro_fil*0.025+0.8*yaw_from_mag_fil)/0.8

plt.figure(figsize=(15,10))
plt.plot(time, yaw_from_mag, label='Magnetic_Field')
plt.plot(time, yaw_from_gyro, label='GYRO')
plt.plot(time, yaw_com, label='Combined')
plt.xlabel('Time(s)', fontsize=15)
plt.ylabel('YAW Angle(rad)')
plt.legend()
plt.suptitle('The Comparison Of YAW Angle', fontsize=20)
plt.savefig('The Comparison Of YAW Angle.png')
plt.show()

for i in range(0,len(yawc)):
    yawc[i] = radians(yawc[i])
yawc = np.unwrap(yawc)
plt.figure(figsize=(15,10))
plt.plot(timeic, yawc, label='IMU Computed')
plt.plot(time, yaw_com, label='Combined')
plt.xlabel('Time(s)', fontsize=15)
plt.ylabel('YAW Angle(rad)')
plt.legend()
plt.suptitle('The Comparison Of YAW Angle', fontsize=20)
plt.savefig('The Comparison Of YAW Angle (Combined and IMU Computed).png')
plt.show()
#%% ORIGINAL IMU LOOP
filenameIL = os.path.join(os.path.dirname(__file__),'Imu_Loop.yaml').replace("\\","/")
IL = list(yaml.safe_load_all(open(filenameIL)))
filenameML = os.path.join(os.path.dirname(__file__),'Mag_Loop.yaml').replace("\\","/")
ML = list(yaml.safe_load_all(open(filenameML)))
accx = []
accy = []
accz = []
angx = []
angy = []
angz = []
magx = []
magy = []
magz = []
yawl = []
pitchl = []
rolll = []
timeil = []
for i in range(0,len(IL)-1):
    accx.append(float(IL[i]['linear_acceleration']['x']))
    accy.append(float(IL[i]['linear_acceleration']['y']))
    accz.append(float(IL[i]['linear_acceleration']['z']))
    angx.append(float(IL[i]['angular_velocity']['x']))
    angy.append(float(IL[i]['angular_velocity']['y']))
    angz.append(float(IL[i]['angular_velocity']['z']))
    yawl.append(float(IL[i]['yaw']))
    pitchl.append(float(IL[i]['pitch']))
    rolll.append(float(IL[i]['roll']))
    timeil.append(i/40)
for i in range(0,len(ML)-1):
    magx.append(float(ML[i]['magnetic_field']['x']))
    magy.append(float(ML[i]['magnetic_field']['y']))
    magz.append(float(ML[i]['magnetic_field']['z']))

calibration = [(max(accx)+min(accx))/2, (max(accy)+min(accy))/2, (max(accz)+min(accz))/2, (max(angx)+min(angx))/2, (max(angy)+min(angy))/2, (max(angz)+min(angz))/2, (max(magx)+min(magx))/2, (max(magy)+min(magy))/2, (max(magz)+min(magz))/2]

cal_accx = [x - calibration[0] for x in accx]
cal_accy = [y - calibration[1] for y in accy]
cal_accz = [z - calibration[2] for z in accz]
cal_angx = [x - calibration[3] for x in angx]
cal_angy = [y - calibration[4] for y in angy]
cal_angz = [z - calibration[5] for z in angz]
cal_magx = [x - calibration[6] for x in magx]
cal_magy = [y - calibration[7] for y in magy]
cal_magz = [z - calibration[8] for z in magz]

hb, ha = signal.butter(1, 0.1, 'highpass', fs=40)
lb, la = signal.butter(1, 1, 'lowpass', fs=40)
accx_fil = signal.filtfilt(hb,ha,cal_accx)
accx_fil = signal.filtfilt(lb,la,accx_fil)
accy_fil = signal.filtfilt(hb,ha,cal_accy)
accy_fil = signal.filtfilt(lb,la,accy_fil)

v_x = integrate.cumtrapz(accx_fil, timeil)
wX = []
angz_A = np.array(cal_angz)
for i in range(0,len(IL)-2):
    wX.append(angz_A[i]*v_x[i])

y_obs = accy_fil

# time = np.linspace(0, 488.075, 19523)
# plt.figure(figsize=(15,10))
# plt.plot(time, wX, label='wX')
# plt.plot(timeil, y_obs, label='y_obs')
# plt.xlabel('wX', fontsize=15)
# plt.ylabel('y_obs', fontsize=15)
# plt.legend()
# plt.suptitle('The Comparison Between wX With y_obs', fontsize=20)
# plt.savefig('The Comparison Between wX With y_obs.png')
# plt.show()

radius = []
semi_major = []
semi_minor = []
r = []
for i in range(0,len(cal_magx)):
    radius.append(sqrt(cal_magx[i]**2 + cal_magy[i]**2))
    r.append(sqrt(cal_magx[i]**2 + cal_magy[i]**2))

for i in range(0,10070):
    semi_major.append(max(r))
    semi_minor.append(min(r))
    r.remove(semi_major[i])
    r.remove(semi_minor[i])

index_major = radius.index(semi_major[30])
index_minor = radius.index(semi_minor[30])

theta = atan2(cal_magy[index_major],cal_magx[index_major])
R = [[cos(theta), sin(theta)], [-sin(theta), cos(theta)]]
cal_magx, cal_magy = np.matmul(R, [cal_magx, cal_magy])

scaler = semi_minor[5035]/semi_major[5036]
for i in range(0,len(cal_magx)):
    cal_magx[i] = cal_magx[i]*scaler

R = [[cos(theta), -sin(theta)], [sin(theta), cos(theta)]]
cal_magx, cal_magy = np.matmul(R, [cal_magx, cal_magy])

yaw_from_mag = []
for i in range(0,len(timeil)):
    yaw_from_mag.append(atan2(-cal_magy[i],cal_magx[i]))

vn = []
ve = []
lb, la = signal.butter(1, 0.0001, 'lowpass', fs=40)
v_x = signal.filtfilt(lb,la,v_x)
v3 = signal.filtfilt(lb,la,v3)

for i in range(0,len(yaw_from_mag)-1):
    vn.append(v_x[i]*cos(yaw_from_mag[i+1]))
    ve.append(v_x[i]*sin(yaw_from_mag[i+1]))

timeil.pop()
ixe = signal.detrend(integrate.cumtrapz(ve, timeil))
ixn = signal.detrend(integrate.cumtrapz(vn, timeil))

# plt.figure(figsize=(15,10))
# plt.plot(xe, xn)
# plt.xlabel('UTM_easting', fontsize=15)
# plt.ylabel('UTM_northing', fontsize=15)
# plt.suptitle('UTM Data Estimated By IMU', fontsize=20)
# plt.savefig('UTM Data Estimated By IMU.png')
# plt.show()

vn = []
ve = []

for i in range(0,len(v3)):
    vn.append(v3[i]*cos(yaw_from_mag[i*38]))
    ve.append(v3[i]*sin(yaw_from_mag[i*38]))

time = np.linspace(0, 502, 503)
gxe = signal.detrend(integrate.cumtrapz(ve, time))
gxn = signal.detrend(integrate.cumtrapz(vn, time))

gxe = gxe*2
gxn = gxn*2
a = abs(gxe[0] - ixe[0])
b = abs(gxn[0] - ixn[0])
for i in range(0,len(ixe)):
    ixe[i] = ixe[i] - a
    ixn[i] = ixn[i] + b

plt.figure(figsize=(15,10))
plt.plot(gxe, gxn, label='GPS')
plt.plot(ixe, ixn, label='IMU')
plt.xlabel('UTM_easting', fontsize=15)
plt.ylabel('UTM_northing', fontsize=15)
plt.suptitle('UTM Data Estimated By GPS And IMU', fontsize=20)
plt.legend()
plt.savefig('UTM Data Estimated By GPS And IMU.png')
plt.show()

timeimu = np.linspace(1, 415,16560)
w = cal_angz[1800:18360]
w_dot = [w[0]/timeimu[0]]
for i in range(1,len(w)):
    w_dot.append((w[i]-w[i-1])/(timeimu[i]-timeimu[i-1]))

zzz = []
for i in range(0,len(w)):
    zzz.append(w_dot[i]+w[i]**2)

imu_acc = cal_accx[1800:18360]
imu_acc_mv = accx_fil[1800:18360]
value = wX[1800:18360]
xc = []
for i in range(0,len(zzz)):
    xc.append((imu_acc[i]-imu_acc_mv[i]-value[i])/zzz[i])

plt.figure(figsize=(15,10))
plt.plot(timeimu, xc)
plt.xlabel('Time(s)', fontsize=15)
plt.ylabel('Xc', fontsize=15)
plt.suptitle('Changes Of The Position Of The Sensor Over Time', fontsize=20)
plt.savefig('Changes Of The Position Of The Sensor Over Time.png')
plt.show()